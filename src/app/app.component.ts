import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AlertsService } from 'angular-alert-module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  username: FormControl;
  password: FormControl;

  showError = false;

  constructor(private alerts: AlertsService) {

  }


  ngOnInit() {
    console.log('application is working');
    this.username = new FormControl('', [Validators.required, Validators.minLength(3)]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(6)]);

    this.username.valueChanges.subscribe((value) => console.log(value));
    this.username.statusChanges.subscribe((status) => {
      status === 'VALID'
      ? this.showError = false
      : this.showError = true;
    });

    this.password.valueChanges.subscribe((value) => console.log(value));
    this.password.statusChanges.subscribe((status) => {
      status === 'VALID'
        ? this.showError = false
        : this.showError = true;
    });
  }

  loginUser() {
    this.alerts.setMessage(`hello ${this.username.value}`,'success');
    console.log(this.username.value);
    console.log(this.password.value);
  }
}
